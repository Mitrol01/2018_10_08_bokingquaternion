﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.XR;

public class HelmetInfo : MonoBehaviour {

    public Transform m_head;
    public XRNode m_tracked;

	void Update () {
        m_head.localPosition = InputTracking.GetLocalPosition(m_tracked);
        m_head.localRotation = InputTracking.GetLocalRotation(m_tracked);
    }
}
