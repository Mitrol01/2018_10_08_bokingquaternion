﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ReplacementCartesien : MonoBehaviour {

    public Transform m_pointOrigine;
    public Transform m_pointCible;
    public Transform m_pointReference;

    // Use this for initialization
    void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {


        m_pointReference.forward = m_pointOrigine.InverseTransformDirection(m_pointCible.forward);
        m_pointReference.position = m_pointOrigine.InverseTransformPoint(m_pointCible.position);
    }
}
