﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.IO;
using System.Text.RegularExpressions;
using UnityEngine;

public class TransformReplayRecorder : MonoBehaviour
{

    public Transform m_target;
    public bool m_recording;
    public float m_frameTime = 0.1f;

    public FramesRecord m_framesRecord;


    IEnumerator Start()
    {

        while (true)
        {

            if (m_recording)
            {

                if (m_frameTime > 0f)
                    yield return new WaitForSeconds(m_frameTime);
                else yield return new WaitForEndOfFrame();
                m_framesRecord.m_frames.Add(
                    new FrameTransform()
                    {
                        m_position = m_target.position,
                        m_rotation = m_target.rotation,
                        m_time = GetTime()
                    });

            }
            yield return new WaitForEndOfFrame();
        }

    }
    public void OnDestroy()
    {
        if (enabled)
            Save();
    }

    private void Save()
    {
        m_framesRecord.Save();
    }

    public float GetTime() { return Time.timeSinceLevelLoad; }

}

[System.Serializable]
public class FrameTransform
{
    public float m_time;
    public Vector3 m_position;
    public Quaternion m_rotation = Quaternion.identity;

}

[System.Serializable]
public class FramesRecord
{

    public string m_name = "Default";
    public List<FrameTransform> m_frames = new List<FrameTransform>();


    public void ConvertAsValide()
    {

        Regex rgx = new Regex("[^a-zA-Z0-9]");
        m_name = rgx.Replace(m_name, "");
    }

    internal float GetDuration()
    {
        if (m_frames.Count <= 0)
            return 0;
        return m_frames[m_frames.Count - 1].m_time;
    }

    public void Normalized()
    {
        if (m_frames.Count <= 0) return;

        float timeStart = m_frames[0].m_time;
        for (int i = 0; i < m_frames.Count; i++)
        {
            m_frames[i].m_time -= timeStart;
        }
    }


    public string GetDirectory() { return Application.dataPath + "/TransformReplay/Records/"; }
    public void Save()
    {
        ConvertAsValide();
        string directory = GetDirectory();
        string jsonSav = JsonUtility.ToJson(this);
        if (!Directory.Exists(directory))
            Directory.CreateDirectory(directory);
        string path = directory + m_name + ".json";
        File.WriteAllText(path, jsonSav);
#if UNITY_EDITOR
        UnityEditor.AssetDatabase.Refresh();
#endif
    }

    public void Load()
    {
        string directory = GetDirectory();
        string path = directory + m_name + ".json";
        if (File.Exists(path))
        {
            FramesRecord recLoad = LoadFrom(File.ReadAllText(path));
            this.m_name = recLoad.m_name;
            this.m_frames = recLoad.m_frames;
        }

    }

    public static FramesRecord LoadFrom(string text)
    {
        return JsonUtility.FromJson<FramesRecord>(text);
    }
}