﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GizmoViewer : MonoBehaviour {

    public Transform m_gizmo;
    public float m_range=5;
    public float m_timeDisplay=5;
	// Use this for initialization
	void Update () {

        DrawMeTheGizmo();

    }

    // Update is called once per frame
    void OnValidate ()
    {
        DrawMeTheGizmo();

    }

    private void DrawMeTheGizmo()
    {
        Debug.DrawLine(m_gizmo.position, m_gizmo.position + m_gizmo.forward * m_range, Color.blue, m_timeDisplay);
        Debug.DrawLine(m_gizmo.position, m_gizmo.position + m_gizmo.up * m_range, Color.green, m_timeDisplay);
        Debug.DrawLine(m_gizmo.position, m_gizmo.position + m_gizmo.right * m_range, Color.red, m_timeDisplay);
    }

    private void Reset()
    {
        m_gizmo = transform;

    }
}
