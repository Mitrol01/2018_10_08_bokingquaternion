﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class HeadAsRootMirorred : MonoBehaviour
{

    public Transform m_toLeftHandAffect;
    public Transform m_toRightHandAffect;

    public Transform m_root;
    public Transform m_head;
    public Transform m_leftHand;
    public Transform m_rightHand;
    

    void Update()
{
    m_toLeftHandAffect.parent = m_root;


    //STUDENT NOTE: Grimbal Lock Affect 
    //Vector3 localPosFromHead = m_head.InverseTransformPoint(m_leftHand.position);
    //Vector3 localDirFromHead = m_head.InverseTransformDirection(m_leftHand.forward);
    //m_toLeftHandAffect.localPosition = localPosFromHead;
    //m_toLeftHandAffect.forward = m_root.TransformDirection( localDirFromHead);

    SetHandPosition(m_leftHand, m_toLeftHandAffect);
    SetHandPosition(m_rightHand, m_toRightHandAffect);

}

    private void SetHandPosition(Transform handTracked, Transform handAffected)
    {
        Vector3 localPosFromHead = m_head.InverseTransformPoint(handTracked.position);
        Quaternion localRotFromHead = Quaternion.Inverse(m_head.rotation) * handTracked.rotation;
        //STUDENT NOTE: Commutativité du Quaternion
        // localRotFromHead =  handTracked.rotation * Quaternion.Inverse(m_head.rotation);


        localPosFromHead.x = -localPosFromHead.x;
        localPosFromHead.z = -localPosFromHead.z;
        localRotFromHead = Quaternion.Euler(0, 180f, 0) * localRotFromHead;

            handAffected.localPosition = localPosFromHead;
            handAffected.localRotation = localRotFromHead;
            
        
        

    }
    
}