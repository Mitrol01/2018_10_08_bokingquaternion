﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ScriptTestStudents : MonoBehaviour {

    public Transform m_root;
    public Vector3 m_angleOne;
    public Vector3 m_angleTwo;
    public bool m_useCommutativity;

    public float m_angle;

    [Range(0,1)]
    public float m_pourcent;

   
	void Update () {
        Quaternion rotation = Quaternion.identity;
        if (m_useCommutativity) {

            Quaternion r = Quaternion.Euler(m_angleOne)
            * Quaternion.Euler( m_angleTwo);
            Quaternion ri = Quaternion.Inverse(r);
            rotation = Quaternion.Lerp(ri, r, m_pourcent);

        }
        else
        {
            Quaternion r = Quaternion.Euler(m_angleTwo)
            * Quaternion.Euler(m_angleOne);
            Quaternion ri = Quaternion.Inverse(r);
            rotation = Quaternion.Lerp(ri,r, m_pourcent) ;

        }

        m_root.localRotation = rotation;
       


       m_angle = Quaternion.Angle(Quaternion.identity, m_root.localRotation);


        Debug.DrawLine(Vector3.zero, Vector3.forward * 5, Color.blue);


    }
}
